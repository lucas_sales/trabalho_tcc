package br.ufc.quixada.tcc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufc.quixada.tcc.model.ArCondicionado;
import br.ufc.quixada.tcc.model.DadosArCondicionadoJson;
import br.ufc.quixada.tcc.mqtt.MqttPublish;
import br.ufc.quixada.tcc.service.ArCondicionadoService;
import br.ufc.quixada.tcc.service.MensagemService;
import br.ufc.quixada.tcc.util.Constants;

@Controller
@RequestMapping(value = {"/ar_condicionado"})
public class ArCondicionadoContrller {
	
	private static final String AR_CONDICIONADO_DESLIGADO = "AR_CONDICIONADO_DESLIGADO";

	private static final String AR_CONDICIONADO_LIGADO = "AR_CONDICIONADO_LIGADO";

	@Autowired
	private ArCondicionadoService arCondicionadoService;
	
	@Autowired
	private MqttPublish pubAndSub;
	
	@Autowired
	private MensagemService mensagemService;
	
	@RequestMapping(value = {"/ligar_desligar_ar_condicionado/{idArCondicioando}/{idSala}"}, method = RequestMethod.GET)
	public String ligarObjeto(@PathVariable String idArCondicioando, @PathVariable String idSala, RedirectAttributes redirect){
		
		Long idArCond = Long.parseLong(idArCondicioando);
		ArCondicionado arCondicionado = arCondicionadoService.getArCondicionado(idArCond);
		
		if(arCondicionado.isEstado()){
			arCondicionado.setEstado(false);
			arCondicionadoService.saveOrEdit(arCondicionado);
			pubAndSub.publicarTopico(Constants.DESLIGAR, arCondicionado.getLocal());
			redirect.addFlashAttribute("desligado", mensagemService.getMessage(AR_CONDICIONADO_DESLIGADO));
		}else{
			arCondicionado.setEstado(true);
			arCondicionadoService.saveOrEdit(arCondicionado);
			pubAndSub.publicarTopico(Constants.LIGAR, arCondicionado.getLocal());
			redirect.addFlashAttribute("ligado", mensagemService.getMessage(AR_CONDICIONADO_LIGADO));
		}

		return "redirect:/sala/detalhe_sala/"+idSala;
	}
	
	@RequestMapping(value = {"/alterar_temperatura"}, method = RequestMethod.POST)
	public String alterarTemperatura(@RequestParam String idSala, @RequestParam String idArCondicionado,
			@RequestParam String temperatura){
		Long idArCond = Long.parseLong(idArCondicionado);
		ArCondicionado arCondicionado = arCondicionadoService.getArCondicionado(idArCond);
		arCondicionado.setTemperatura(Long.parseLong(temperatura));
		arCondicionadoService.saveOrEdit(arCondicionado);
		
		pubAndSub.publicarTopico("AUMENTAR_TEMPERATURA", arCondicionado.getLocal());
		
		return "redirect:/sala/detalhe_sala/"+idSala;
	}
	
	@RequestMapping(value = "/aumentar",method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String aumentarTemperatura(@RequestBody DadosArCondicionadoJson dados){
		ArCondicionado arCondicionado = arCondicionadoService.getArCondicionado(dados.getIdArcondicionado());
		arCondicionado.setTemperatura(dados.getTemperatura());
		arCondicionadoService.saveOrEdit(arCondicionado);
		pubAndSub.publicarTopico("AUMENTAR_TEMPERATURA", arCondicionado.getLocal());
		return "{\"result\":\"ok\"}";
	}
	
	@RequestMapping(value = "/diminuir",method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String diminuirTemperatura(@RequestBody DadosArCondicionadoJson dados){
		ArCondicionado arCondicionado = arCondicionadoService.getArCondicionado(dados.getIdArcondicionado());
		arCondicionado.setTemperatura(dados.getTemperatura());
		arCondicionadoService.saveOrEdit(arCondicionado);
		pubAndSub.publicarTopico("AUMENTAR_TEMPERATURA", arCondicionado.getLocal());
		return "{\"result\":\"ok\"}";
	}
}