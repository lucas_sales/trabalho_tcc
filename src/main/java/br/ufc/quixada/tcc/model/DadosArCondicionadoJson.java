package br.ufc.quixada.tcc.model;

public class DadosArCondicionadoJson {
	private Long idArcondicionado;
	private Long temperatura;
	public Long getIdArcondicionado() {
		return idArcondicionado;
	}
	public void setIdArcondicionado(Long idArcondicionado) {
		this.idArcondicionado = idArcondicionado;
	}
	public Long getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Long temperatura) {
		this.temperatura = temperatura;
	}
}