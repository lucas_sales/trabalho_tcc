package br.ufc.quixada.tcc.model;

import org.springframework.security.core.GrantedAuthority;

public enum Papel implements GrantedAuthority{
	PROFESSOR("Professor"), STI("Sti");
	
	private String descricao;

	private Papel(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String getAuthority() {
		return this.toString();
	}
}