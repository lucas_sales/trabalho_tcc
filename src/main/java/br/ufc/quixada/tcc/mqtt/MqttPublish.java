package br.ufc.quixada.tcc.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.quixada.tcc.util.Constants;

@Service
public class MqttPublish implements MqttCallback{

	
    private String topic        = "sala/#";
    private int qos             = 2;
    private String ip = "138.197.32.26";
    private String ipUFC = "192.168.1.4";
    private String broker       = "tcp://"+ip+":1883";
    private String clientId     = "JavaSample";
    private MemoryPersistence persistence = new MemoryPersistence();
    private MqttClient client = null;
    
    @Autowired
    public MqttPublish(){
    	 try {
			if(this.client == null || !(this.client.isConnected())){
				this.client = new MqttClient(broker, clientId, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				
				connOpts.setCleanSession(true);
				System.out.println("Connecting to broker: "+broker);
				this.client.connect(connOpts);
				System.out.println("Connected");
				this.client.setCallback(this);
				
				//subscribing todos os topicos
				subscribingTopic("UFC/CAMPUS_QXD");
			}
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void publicar(String mensagem){
        try {
            MqttMessage message = new MqttMessage(mensagem.getBytes());
            message.setQos(qos);
            client.publish(topic, message);
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
	}
    public void publicarTopico(String mensagem, String topico){
        try {
            MqttMessage message = new MqttMessage(mensagem.getBytes());
            message.setQos(qos);
            client.publish(topico, message);
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
	}
    
    public void subscribing() {
        try {
        	if(client.isConnected()){
        		client.subscribe("resposta");
        		//client.subscribe("foo");
        		//client.s
        	}
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
    
    public void subscribingTopic(String topic) {
        try {
        	if(client.isConnected()){
        		client.subscribe(topic);
        	}
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
    
	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		System.out.println(arg0.getMessageId());
	}

	@Override
	public void messageArrived(String topico, MqttMessage mensagem) throws Exception {
		System.out.println("TOPICO: "+ topico);
		System.out.println("Menssagem: " + mensagem.toString());
		if(mensagem.equals("LIGOU")){
			System.out.println("##############################################");
			System.out.println("MANDOU MSG DE VOLTA");
			System.out.println("##############################################");
			//publicarTopico(mensagem.toString(), Constants.LIGAR);
		}else if(mensagem.equals(Constants.DESLIGADA)){
			publicarTopico(mensagem.toString(), Constants.DESLIGAR);
		}

	}
}
