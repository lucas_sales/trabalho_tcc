package br.ufc.quixada.tcc.repository;

import java.sql.Time;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.ufc.quixada.tcc.model.Sala;

@Repository
@Transactional
public interface SalaRepository extends CrudRepository<Sala, Long>{
	public List<Sala> findSalaByBlocoId(Long idBloco);
	public List<Sala> findSalaByManhaLigar(Time ligarManha);
	public List<Sala> findSalaByManhaDesligar(Time desligarManha);
	public List<Sala> findSalaByTardeLigar(Time ligarTarde);
	public List<Sala> findSalaByTardeDesligar(Time desligarTarde);
	public List<Sala> findSalaByNoiteLigar(Time ligarNoite);
	public List<Sala> findSalaByNoiteDesligar(Time desligarNoite);
	public Sala findSalaById(Long idSala);
	public Sala findSalaByBlocoIdAndNome(Long idBloco, String nomeSala);
}