package br.ufc.quixada.tcc.schedule;

import java.sql.Time;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.ufc.quixada.tcc.model.ArCondicionado;
import br.ufc.quixada.tcc.model.Lampada;
import br.ufc.quixada.tcc.model.Sala;
import br.ufc.quixada.tcc.mqtt.MqttPublish;
import br.ufc.quixada.tcc.service.ArCondicionadoService;
import br.ufc.quixada.tcc.service.LampadaService;
import br.ufc.quixada.tcc.service.ProjetorService;
import br.ufc.quixada.tcc.service.SalaService;
import br.ufc.quixada.tcc.util.Constants;

@Component
@Transactional
public class ScheduledTasks {
	
	private static final String MANHA_8 = "08:00:00";
	private static final String MANHA_10 = "10:00:00";
	private static final String MANHA_12 = "12:00:00";
	private static final String TARDE_13 = "13:30:00";
	private static final String TARDE_15 = "15:30:00";
	private static final String TARDE_17 = "17:30:00";
	private static final String NOITE_18 = "18:00:00";
	private static final String NOITE_20 = "20:00:00";
	private static final String NOITE_22 = "22:00:00";
	
	private Time time;
	
	@Autowired
	private MqttPublish mqtt;
	
	@Autowired
	private SalaService salaService;
	@Autowired
	private ArCondicionadoService arcondicionadoService;
	@Autowired
	private LampadaService lampadaService;
	@Autowired
	private ProjetorService projetorService;
	

	@Scheduled(cron = "0 */1 * * * MON-FRI")
	public void teste() {
    	List<Sala> salas = salaService.getAll();
    	for(Sala s : salas){
    		System.out.println("######################");
    		System.out.println("AGENDAMENTO");
    		System.out.println("######################");
    		for(Lampada l : s.getLampadas()){
    			if(l.isEstado()){
    				
    				mqtt.publicarTopico(Constants.DESLIGAR, l.getLocal());
    				l.setEstado(false);
    				lampadaService.saveOrEdit(l);
    			}
    			else{
    				mqtt.publicarTopico(Constants.LIGAR, l.getLocal());
    				l.setEstado(true);
    				lampadaService.saveOrEdit(l);
    			}
    		}
    	}
    }
	
    @Scheduled(cron = "0 0 8 * * MON-FRI")
	public void ligarObejtosManhaAs8() {
    	time= Time.valueOf(MANHA_8);
    	ligarManha(time);
    }
    
    @Scheduled(cron = "0 0 10 * * MON-FRI")
	public void ligarEDesligarObejtosManhaManhaAs10() {
    	time= Time.valueOf(MANHA_10);
    	ligarManha(time);
    	desligarManha(time);
    }
    
    @Scheduled(cron = "0 0 12 * * MON-FRI")
	public void desligarObjetosManhaAs12() {
    	time= Time.valueOf(MANHA_12);
    	desligarManha(time);
    }
    
    @Scheduled(cron = "0 */30 13 * * MON-FRI")
    public void ligarObjetosTardeAs13(){
    	time = Time.valueOf(TARDE_13);
    	ligarTarde(time);
    }
    
    @Scheduled(cron = "0 */30 15 * * MON-FRI")
    public void ligarEDesligarObjetosTardeAs15(){
    	time = Time.valueOf(TARDE_15);
    	ligarTarde(time);
    	desligarTarde(time);
    }
    
    @Scheduled(cron = "0 */30 17 * * MON-FRI")
    public void desligarObjetosTardeAs17(){
    	time = Time.valueOf(TARDE_17);
    	desligarTarde(time);
    }
    
    @Scheduled(cron = "0 0 18 * * MON-FRI")
    public void ligarObjetosNoiteAs18(){
    	time = Time.valueOf(NOITE_18);
    	ligarNoite(time);
    }
    
    @Scheduled(cron = "0 0 20 * * MON-FRI")
    public void ligarEDesligarObjetosNoiteAs20(){
    	time = Time.valueOf(NOITE_20);
    	ligarNoite(time);
    	desligarNoite(time);
    }
    
    @Scheduled(cron = "0 0 22 * * MON-FRI")
    public void desligarObjetosNoiteAs22(){
    	time = Time.valueOf(NOITE_22);
    	desligarNoite(time);
    }
    
    public void ligarManha(Time time){
    	List<Sala> salas = salaService.getSalasParaLigarDeManha(time);
    	for(Sala s : salas){
    		if(!s.getProjetor().isEstado()){
    			mqtt.publicarTopico(Constants.LIGAR, s.getProjetor().getLocal());
    			s.getProjetor().setEstado(true);
    			projetorService.saveOrEdit(s.getProjetor());
    		}
    		publicarArCondicionadosLigar(s.getArCondicionados());
    		publicarLampadaLigar(s.getLampadas());
    	}
    }
    
    public void ligarTarde(Time time){
    	List<Sala> salas = salaService.getSalasParaLigarDeTarde(time);
    	for(Sala s : salas){
    		if(!s.getProjetor().isEstado()){
    			mqtt.publicarTopico(Constants.LIGAR, s.getProjetor().getLocal());
    			s.getProjetor().setEstado(true);
    			projetorService.saveOrEdit(s.getProjetor());
    		}
    		publicarArCondicionadosLigar(s.getArCondicionados());
    		publicarLampadaLigar(s.getLampadas());
    	}
    }
    
    public void ligarNoite(Time time){
    	List<Sala> salas = salaService.getSalasParaLigarDeNoite(time);
    	for(Sala s : salas){
    		if(!s.getProjetor().isEstado()){
    			mqtt.publicarTopico(Constants.LIGAR, s.getProjetor().getLocal());
    			s.getProjetor().setEstado(true);
    			projetorService.saveOrEdit(s.getProjetor());
    		}
    		publicarArCondicionadosLigar(s.getArCondicionados());
    		publicarLampadaLigar(s.getLampadas());
    	}
    }
    
    public void desligarManha(Time time){
    	List<Sala> salas = salaService.getSalasParaDesligarDeManha(time);
    	for(Sala s : salas){
    		if(s.getProjetor().isEstado()){
    			mqtt.publicarTopico(Constants.DESLIGAR, s.getProjetor().getLocal());
    			s.getProjetor().setEstado(false);
    			projetorService.saveOrEdit(s.getProjetor());
    		}
    		publicarArCondicionadosDeligar(s.getArCondicionados());
    		publicarLampadaDesligar(s.getLampadas());
    	}
    }
    
    public void desligarTarde(Time time){
    	List<Sala> salas = salaService.getSalasParaDesligarDeTarde(time);
    	for(Sala s : salas){
    		if(s.getProjetor().isEstado()){
    			mqtt.publicarTopico(Constants.DESLIGAR, s.getProjetor().getLocal());
    			s.getProjetor().setEstado(false);
    			projetorService.saveOrEdit(s.getProjetor());
    		}
    		publicarArCondicionadosDeligar(s.getArCondicionados());
    		publicarLampadaDesligar(s.getLampadas());
    	}
    }
    
    public void desligarNoite(Time time){
    	List<Sala> salas = salaService.getSalasParaDesligarDeNoite(time);
    	for(Sala s : salas){
    		if(s.getProjetor().isEstado()){
    			mqtt.publicarTopico(Constants.DESLIGAR, s.getProjetor().getLocal());
    			s.getProjetor().setEstado(false);
    			projetorService.saveOrEdit(s.getProjetor());
    		}
    		publicarArCondicionadosDeligar(s.getArCondicionados());
    		publicarLampadaDesligar(s.getLampadas());
    	}
    }
    
    public void publicarArCondicionadosLigar(List<ArCondicionado> listaArcondicionado){
    	for(ArCondicionado c : listaArcondicionado){
    		if(!c.isEstado()){
    			mqtt.publicarTopico(Constants.LIGAR, c.getLocal());
    			c.setEstado(true);
    			arcondicionadoService.saveOrEdit(c);
    		}
    			
    	}
    }
    
    public void publicarLampadaLigar(List<Lampada> listaLampadas){
    	for(Lampada l : listaLampadas){
    		if(!l.isEstado()){
    			mqtt.publicarTopico(Constants.LIGAR, l.getLocal());
    			l.setEstado(true);
    			lampadaService.saveOrEdit(l);
    		}
    			
    	}
    }
    
    public void publicarArCondicionadosDeligar(List<ArCondicionado> listaArcondicionado){
    	for(ArCondicionado c : listaArcondicionado){
    		if(c.isEstado()){
    			mqtt.publicarTopico(Constants.DESLIGAR, c.getLocal());
    			c.setEstado(false);
    			arcondicionadoService.saveOrEdit(c);
    		}
    			
    	}
    }
    
    public void publicarLampadaDesligar(List<Lampada> listaLampadas){
    	for(Lampada l : listaLampadas){
    		if(l.isEstado()){
    			mqtt.publicarTopico(Constants.DESLIGAR, l.getLocal());
    			l.setEstado(false);
    			lampadaService.saveOrEdit(l);
    		}
    	}
    }
}