package br.ufc.quixada.tcc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.quixada.tcc.model.ArCondicionado;
import br.ufc.quixada.tcc.repository.ArCondicionadoRepository;

@Service
public class ArCondicionadoService {
	
	@Autowired
	private ArCondicionadoRepository arCondicionadoRepository;
	
	public void saveOrEdit(ArCondicionado arCondicionado){
		arCondicionadoRepository.save(arCondicionado);
	}
	public ArCondicionado getArCondicionado(Long idArCondicionado){
		return arCondicionadoRepository.findByIdOrderById(idArCondicionado);
	}
}