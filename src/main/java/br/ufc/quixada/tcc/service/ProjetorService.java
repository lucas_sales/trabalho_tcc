package br.ufc.quixada.tcc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufc.quixada.tcc.model.Projetor;
import br.ufc.quixada.tcc.repository.ProjetorRepository;

@Service
public class ProjetorService {
	
	@Autowired
	private ProjetorRepository projetorRepository;
	
	public void saveOrEdit(Projetor projetor){
		projetorRepository.save(projetor);
	}
	
	public Projetor getProjetor(Long idProjetor){
		return projetorRepository.findProjetorById(idProjetor);
	}
}